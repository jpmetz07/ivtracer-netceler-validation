#!/bin/sh
# $1 est le numero de version a builder
#faire un build.sh avant

IVT_TEMPLATE_FOLDER=target/ivtracer-config-template

rm -r target/IVTracer-config-patch-$1
rm  target/IVTracer-config-patch-$1.zip
mkdir -p target/IVTracer-config-patch-$1

mkdir -p target/IVTracer-config-patch-$1/engine/infocenter/deploy
cp -r $IVT_TEMPLATE_FOLDER/engine/infocenter/deploy/* target/IVTracer-config-patch-$1/engine/infocenter/deploy

cp applyIVTracerPatch.bat target/IVTracer-config-patch-$1/
sed -i 's/%VERSION%/'$1'/g' target/IVTracer-config-patch-$1/applyIVTracerPatch.bat

cd target
zip -r IVTracer-config-patch-$1.zip IVTracer-config-patch-$1